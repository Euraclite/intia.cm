<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Assurance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Assurance controller.
 *
 */
class AssuranceController extends Controller
{
     /**
     * Lists all assurances entities.
     * @Route("/assurance", name="assurance_index")
     * 
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $assurances = $em->getRepository('AppBundle:Assurance')->findAll();

        return $this->render('assurance/index.html.twig', array(
            'assurances' => $assurances,
        ));
    }

    /**
     * Creates a new assurances entity.
     * @Route("/assurances/new", name="assurance_new")
     * 
     */
    public function newAction(Request $request)
    {
        $assurance = new Assurance();
        $form = $this->createForm('AppBundle\Form\AssuranceType', $assurance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($assurance);
            $em->flush();

            return $this->redirectToRoute('assurance_show', array('id' => $assurance->getId()));
        }

        return $this->render('assurance/new.html.twig', array(
            'assurance' => $assurance,
            'form' => $form->createView(),
        ));
    }

     /**
     * Finds and displays a assurances entity.
     * @Route("/assurance/{id}/show", name="assurance_show")
     */
    public function showAction(Assurance $assurance)
    {
        $deleteForm = $this->createDeleteForm($assurance);

        return $this->render('assurance/show.html.twig', array(
            'assurance' => $assurance,
            'delete_form' => $deleteForm->createView(),
        ));
    }

     /**
     * Displays a form to edit an existing fichier entity.
     * @Route("/assurance/{id}/edit", name="assurance_edit")
     *
     */
    public function editAction(Request $request, Assurance $assurance)
    {
        $deleteForm = $this->createDeleteForm($assurance);
        $editForm = $this->createForm('AppBundle\Form\AssuranceType', $assurance);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('assurance_edit', array('id' => $assurance->getId()));
        }

        return $this->render('assurance/edit.html.twig', array(
            'assurance' => $assurance,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a assurances entity.
     *  @Route("/assurance/{id}/delete", name="assurance_delete")
     * 
     */
    public function deleteAction(Request $request, Assurance $assurance)
    {
        $form = $this->createDeleteForm($assurance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($assurance);
            $em->flush();
        }

        return $this->redirectToRoute('assurance_index');
    }

    /**
     * Creates a form to delete a assurance entity.
     *
     * @param Assurance $assurance The assurance entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Assurance $assurance)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('assurance_delete', array('id' => $assurance->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
